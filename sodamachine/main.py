#!/usr/bin/python
## Built-in modules
import os
import sys
import time
import pymysql
import serial
from threading import Thread
##

## SodaMachine modules
# To import from the admin directory
from sodamachine.admin import *
# To import LCD object from the sodalcd module
from sodamachine.sodalcd import LCD
# To import Magstrip object from sodamag module
from sodamachine.sodamag import MagStrip
##

def main():
    ## Variables
    # Default account balance
    accountBalance = 0
    # Default char received data
    charReceived = ""
    # Cost of one soda
    sodaCost = .50 # 50 cents
    # Common break after final LCD output
    BREAK = 5 
    # Single sleeps to prevent CPU overload
    STEP = 1
    ##
    
    ## Serial/USB connections
    # Initialize and connect to Arduino
    ser = serial.Serial('/dev/arduino', 9600, timeout=1)

    # Initialize and connect to LCD
    lcd = LCD(0x0403, 0xc630)
    
    # Initialize and connect to Magstrip
    mag = MagStrip('/dev/cardreader')
    ##

    ## Threaded processes
    # Start magstrip thread
    p = Thread(target=mag.read, args=())
    p.daemon = True
    p.start()
    ##
    
    # Main program loop
    while True:
        # Display main message and current time
        ct = time.strftime("%I:%M %P")
        lcd.clear()
        lcd.write('ACM Soda Machine\n    %s' % ct)
        time.sleep(STEP)

        # Only process when card swiped
        if mag.studentID:
            charReceived = ""
            # Check the database for the studentID
            lcd.clear()
            lcd.write('Authorizing ...')
            result = checkUser.main(mag.studentID)
            time.sleep(3)

            # If result is null then not valid user
            if not result:
                # Output invalid user to LCD
                lcd.clear()
                lcd.write('No account found\nfor ID %s' % mag.studentID)
                time.sleep(BREAK)
            else:
                # Valid user
                accountBalance = int(result[2]) # third record is account balance
                fname = str(result[3]) # fourth record is first name
            
                # Check accountBalance
                # Low balance
                if accountBalance < int(sodaCost * 100):
                    # Output low balance to LCD
                    lcd.clear()
                    lcd.write('Sorry %s' % fname)
                    time.sleep(STEP)
                    lcd.clear()
                    lcd.write('Not enough funds\nfor transaction')
                    time.sleep(BREAK)
                # Sufficient Balance
                else:
                    # Send authorized to Arduino
                    ser.write('a')

                    # Output Make Selection to LCD
                    lcd.clear()
                    lcd.write('Hello %s' % fname)
                    time.sleep(STEP)
                    lcd.clear()
                    lcd.write('Please make\na selection')

                    # Read serial for proper response from Arduino, timeout after 15 seconds
                    charReceived = ser.read(1)
                    timeout = time.time() + 14
                    loop = True
                    while(charReceived != 's' and charReceived != 'e' and loop == True):
                        charReceived = ser.read(1)
                        if time.time() >= timeout:
                            loop = False
                        
                 
                    
                    # Success from Arduino
                    if charReceived == 's':
                        # Deduct sodaCost from amount
                        changeAmount.main(mag.studentID, -sodaCost)

                        # Output success & new balance to LCD
                        lcd.clear()
                        lcd.write('Thank you, %s' % fname)
                        time.sleep(STEP)
                        lcd.clear()
                        lcd.write('Remaining\nBalance: $%s' % accountBalance)
                        time.sleep(BREAK)
                    # Vending error from Arduino
                    elif charReceived == 'e':
                        # Output error to LCD
                        lcd.clear()
                        lcd.write('Error dispensing\nPlease try again')
                        time.sleep(BREAK)
                    # Took too long to make a selection
                    else:
                        lcd.clear()
                        lcd.write('Transaction\ntimed out')
                        time.sleep(BREAK)
            # Reset IDs that were read
            mag.rawStudentID = ""
            mag.studentID = ""

if __name__ == "__main__":
    # Call main function
    main()
