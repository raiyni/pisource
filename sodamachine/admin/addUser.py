import sys
import pymysql
from sodamachine import sodadb

def main(*args):
  # input checking
  if len(args) < 4:
    print "Usage: python add_user.py <ID> <Amount> <FName> <LName> [RFID]"
    return
  ID = int(args[0])
  Amount = float(args[1])
  FName = args[2]
  LName = args[3]
  RFID = None
  verbose = False
  if len(args) > 4:
    RFID = args[4]

  try:
    # connect to MySQL database
    con = sodadb.connect()
    cur = con.cursor()
    
    try:
      # insert into table
      sodadb.addUser(con, cur, ID, RFID, int(Amount * 100), FName, LName)
    except pymysql.Error as e:
      print("Error: could not add user")
      print(str(e))
  except pymysql.Error as e:
    print("Error: could not connect to database")
