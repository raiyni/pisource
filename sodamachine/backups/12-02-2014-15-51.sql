DROP TABLE IF EXISTS `users`;

  CREATE TABLE `users`(
    `id` int(11) NOT NULL,
    `rfid` varchar(11) DEFAULT NULL,
    `balance` int(10) unsigned DEFAULT '0',
    `firstname` varchar(25) DEFAULT NULL,
    `lastname` varchar(40) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    UNIQUE KEY `rfid` (`rfid`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  LOCK TABLES `users` WRITE;
  INSERT INTO `users` VALUES (1233, None, 400, 'd', 'r'),(1233998, '0001', 1000, 'test2', 'test2'),(12339989, '0000', 1000, 'Chris', 'Rawlings'),(12365911, '12312', 1000, 'wer', 't'),(12365913, None, 400, 'r', 'y');UNLOCK TABLES;